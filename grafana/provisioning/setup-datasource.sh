#!/bin/bash
# Copy files from /srv/provisioning/datasources to /etc/grafana/provisioning/datasources
# Note this asumes that grafana is configured with /etc/grafana/provisioning!

source=/srv/provisioning/datasources/
dest=/etc/grafana/provisioning/datasources/

mkdir -p /etc/grafana/provisioning/datasources

# Copy datasource *.yml files ..
for filename in $source*.yml; do
  echo "Importing datasource file: $filename"
  cp --remove-destination $filename $dest
done
for filename in $dest*.yml; do
  echo " - provisioned datasource: $filename"
  chmod 755 $filename
  chown 472:472 $filename
done
