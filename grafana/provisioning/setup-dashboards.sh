#!/bin/bash
# Copy files from /srv/provisioning/dashboards to /etc/grafana/provisioning/dashboards
# Note this asumes that grafana is configured with /etc/grafana/provisioning!

source=/srv/provisioning/dashboards
dest=/etc/grafana/provisioning/dashboards

mkdir -p /etc/grafana/provisioning/dashboards

# Copy dashboards provisioning definition ..
cp --remove-destination $source/dashboards.yml $dest/dashboards.yml
chmod 755 $dest/dashboards.yml
chown 472:472 $dest/dashboards.yml

# Copy dashboards *.json files ..
for filename in $source/*.json; do
  echo "Importing dashboard file: $filename"
  cp --remove-destination $filename $dest/
done
for filename in $dest/*.json; do
  echo " - provisioned dashboard: $filename"
  chmod 755 $filename
  chown 472:472 $filename
done
