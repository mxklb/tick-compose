#!/bin/bash
# Clean up & Initialize fresh local folders ..
data_dir="/srv/docker"

# Set data folder from environment
[[ -z "${DATA_FOLDER}" ]] || data_dir="${DATA_FOLDER}"

# Remove and create folders
sudo rm -rf $data_dir/grafana
sudo rm -rf $data_dir/influxdb
sudo mkdir -p $data_dir/influxdb/data
sudo mkdir -p $data_dir/grafana/data
sudo mkdir -p $data_dir/grafana/provisioning/plugins
sudo mkdir -p $data_dir/grafana/provisioning/notifiers
sudo chown 472:472 $data_dir/grafana/data
sudo chown 472:472 $data_dir/grafana/provisioning
