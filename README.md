# Tick Compose
This is a docker compose file to spin up

- [Telegraf](https://www.influxdata.com/time-series-platform/telegraf)
- [InfluxDB](https://www.influxdata.com/products/influxdb-overview)
- [Grafana](https://grafana.com)

This is a server monitoring solution provided by docker containers. To spin it up call

    docker-compose up -d --build

Telegraf collects docker host metrics and writes them into InfluxDB, while Grafana plots them.

Note: This is successfully tested at Raspberry PI with raspbian, ubuntu-server & HypriotOS and ubuntu desktop.

During first boot, the initial setup for influxdb is performed - wait for it to finish!

# Setup
For initial setup you need to setup some local directories on the docker host.
To persist data from influxdb (*/var/lib/influxdb*) and grafana (*/var/lib/grafana*)
you have to create some folders. The following host directories are mounted by the
*docker-compose.yml*

    sudo mkdir -p $DATA_FOLDER/influxdb/data
    sudo mkdir -p $DATA_FOLDER/grafana/data
    sudo mkdir -p $DATA_FOLDER/grafana/provisioning
    sudo chown 472:472 $DATA_FOLDER/grafana/data
    sudo chown 472:472 $DATA_FOLDER/grafana/provisioning

Set variable `$DATA_FOLDER` in *.env* to change the default data directory which
is */srv/docker*.  


Optionally use the `sudo ./init-local.sh` script to clean & recreate folders.

## Configuration
This docker-compose file configures users and a database. For configuration
environment variables are used. The basic setup works fine with default values -
not setting any variables. Nevertheless its recommended to at least configure the
admin, read & write users for influx and the grafana admin. Therefore
check out ..

    source ./configure-users.sh

This lets you configure the recommended users interactively - sets the variables.

### InfluxDB
The inital InfluxDB setup makes use of the environment variables

- **INFLUXDB_DB** (default=telegraf)
- **INFLUXDB_ADMIN_USER** (default=admin)
- **INFLUXDB_ADMIN_PASSWORD** (default=password)
- **INFLUXDB_READ_USER** (default=influx-reader)
- **INFLUXDB_READ_USER_PASSWORD** (default=password)
- **INFLUXDB_WRITE_USER** (default=influx-writer)
- **INFLUXDB_WRITE_USER_PASSWORD** (default=password)

Check InfluxDB [database initialization](https://docs.docker.com/samples/library/influxdb/).
Set *env.influxdb* to configure your custom setup.

### Telegraf
For telegraf use the following environment variables

- **INFLUXDB_DB** (default=telegraf)
- **INFLUXDB_WRITE_USER** (default=influx-writer)
- **INFLUXDB_WRITE_USER_PASSWORD** (default=password)
- **INFLUXDB_URL** (default="http://influxdb:8086")
- **MQTT_BROKER** (default="ubuntu:1883")

Mqtt broker is an optional setting to collect metrics via MQTT.

Note: The InfluxDB user must have rights to write into the database.
Set *env.telegraf* to configure your custom setup.

### Grafana
Set following environment variables to configure grafana

- **INFLUXDB_DB** (default=telegraf)
- **INFLUXDB_READ_USER** (default=influx-reader)
- **INFLUXDB_READ_USER_PASSWORD** (default=password)
- **INFLUXDB_URL** (default=http://influxdb:8086)
- **GF_SECURITY_ADMIN_USER** (default=admin)
- **GF_SECURITY_ADMIN_PASSWORD** (default=admin)

Note: The InfluxDB user must have rights to read the database.
Set *env.grafana* to configure your custom setup.

Also set the grafana admin password during first login using default admin!

#### Grafana Provisioning
Grafana's provisioning feature is used to setup custom datasources, dashboards
and/or notifiers. Default provisioning is defined within the *grafana/provisioning*
folder. These provisioning files are always available when spinning up this
tick stack. This configures the default database and an os-metrics dashboard.

- *provisioning/datasources/database-default.yml*
- *provisioning/dashboards/dashboards.yml*
- *provisioning/dashboards/os-metrics.json*

To enable further customization's place additional provisioning sources into
the `$DATA_FOLDER` within the following subfolders:

- *$DATA_FOLDER/grafana/provisioning/dashboards/*
- *$DATA_FOLDER/grafana/provisioning/datasources/*
- *$DATA_FOLDER/grafana/provisioning/notifiers/*

These sources will be made available besides the default sources within
the grafana provisioning folder /etc/grafana/provisioning.

Note: Check *docker-compose.yml*. Notifiers are actually not pre defined. Make
sure to choose unused filenames!

**Persist/Save Dashboard Changes**

To persist dashboard changes, use the "*Save Dashboard: COPY/SAVE JSON*" feature
of grafana UI. Then update and commit the dashboard *.json* files. Use
*download-dashboards.sh* to load all dashboards into a local directory.
