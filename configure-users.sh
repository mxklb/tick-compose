#!/bin/bash
# Interactive shell script to set environment variables.
# This sets all users for InfluxDB and the Grafana admin.
# Call like this:$ source ./config-users.sh

echo_user_warning() {
  echo " -- Warning: Username empty -> using default user: $1"
}
echo_password_warning() {
  echo " -- Warning: Password empty -> using default password: $1"
}

# Setup InfluxDB admin user ..
printf "# Setup InfluxDB users ..\n"
read -p ' - Admin username: ' admin_user
if [[ -z "$admin_user" ]]; then
  admin_user=admin
  echo_user_warning $admin_user
fi
read -sp ' - Admin password: ' admin_password
printf "\n"
if [[ -z "$admin_password" ]]; then
  admin_password=password
  echo_password_warning $admin_password
fi

# Setup InfluxDB write user ..
read -p ' - Write username: ' writer_user
if [[ -z "$writer_user" ]]; then
  writer_user=influx-writer
  echo_user_warning $writer_user
fi
read -sp ' - Write password: ' writer_password
printf "\n"
if [[ -z "$writer_password" ]]; then
  writer_password=password
  echo_password_warning $writer_password
fi

# Setup InfluxDB read user ..
read -p ' - Read username: ' reader_user
if [[ -z "$reader_user" ]]; then
  reader_user=influx-reader
  echo_user_warning $reader_user
fi
read -sp ' - Read password: ' reader_password
printf "\n"
if [[ -z "$reader_password" ]]; then
  reader_password=password
  echo_password_warning $reader_password
fi

# Setup Grafana admin user ..
printf "# Setup Grafana users ..\n"
read -p ' - Admin username: ' grafana_admin
if [[ -z "$grafana_admin" ]]; then
  grafana_admin=admin
  echo_user_warning $grafana_admin
fi
read -sp ' - Admin password: ' grafana_password
printf "\n"
if [[ -z "$grafana_password" ]]; then
  grafana_password=admin
  echo_password_warning $grafana_password
fi

export INFLUXDB_ADMIN_USER=$admin_user
export INFLUXDB_ADMIN_PASSWORD=$admin_password
export INFLUXDB_READ_USER=$reader_user
export INFLUXDB_READ_USER_PASSWORD=$reader_password
export INFLUXDB_WRITE_USER=$writer_user
export INFLUXDB_WRITE_USER_PASSWORD=$writer_password
export GF_SECURITY_ADMIN_USER=$grafana_admin
export GF_SECURITY_ADMIN_PASSWORD=$grafana_password
